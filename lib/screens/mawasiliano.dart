import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class Mawasiliano extends StatefulWidget {
  Mawasiliano({Key key}) : super(key: key);

  _MawasilianoState createState() => _MawasilianoState();
}

class _MawasilianoState extends State<Mawasiliano> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: GradientAppBar(
          leading: BackButton(),
          title: Text('Mawasiliano'),
          gradient: LinearGradient(colors: [
            Color(0XFF7f933d),
            Color(0XFF4d7e46),
            Color(0XFF2e5536),
          ]),
        ),
        body: Container(
          // decoration: BoxDecoration(
          //     gradient: LinearGradient(
          //         begin: Alignment.topLeft,
          //         end: Alignment.bottomCenter,
          //         colors: [
          //       Color(0XFF7f933d),
          //       Color(0XFF4d7e46),
          //       Color(0XFF2e5536),
          //       Color(0xFF2c5334),
          //       Color(0XFF294f36)
          //     ])),
          padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
          // alignment: Alignment.bottomCenter,
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.center,
            // crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              ListTile(
                leading: Icon(
                  Icons.call,
                  color:  Color(0XFF294f36),
                ),
                title: Text(
                  '+2552662963277/8',
                  style: TextStyle(color:  Color(0XFF294f36)),
                ),
              ),
              ListTile(
                leading: Icon(Icons.email, color:  Color(0XFF294f36)),
                title: Text('info@roadfund.go.tz',
                    style: TextStyle(color:  Color(0XFF294f36))),
              )
            ],
          ),
        ),
      ),
    );
  }
}
